let creditCard=[];
let nameCard=document.getElementById('name');
let numberCard=document.getElementById('numberCard');
let monthCard=document.getElementById('month');
let yearCard=document.getElementById('year');
let cvc=document.getElementById('cvc');
let submit=document.getElementById('submit');
let form=document.getElementById('form');
let datos={};

let frontCard=document.getElementById('frontCard');
let backCardcvc=document.getElementById('backCardcvc');
let monthCardFront=document.getElementById('monthCardFront');
let yearCardFront=document.getElementById('yearCardFront');
let nameCardFront=document.getElementById('nameCardFront');
let complete=document.querySelector('.complete-state');

let cont=document.getElementById('continue');

/*Guardar datos de la tarjeta*/ 
form.addEventListener('submit',(e)=>{
    e.preventDefault();
    if(nameCard.value==''||numberCard.value==''||monthCard.value=='' || yearCard.value==''||cvc.value==''){
        if(nameCard.value==''){
            nameCard.nextElementSibling.style.display="block";
        }
        else{
            nameCard.nextElementSibling.style.display="none";
        }
        if(numberCard.value==''){
            numberCard.nextElementSibling.style.display='block';
        }
        else{
            numberCard.nextElementSibling.style.display='none';
        }
        if(monthCard.value=='' || yearCard.value==''){
            monthCard.parentNode.nextElementSibling.style.display='block';
        }
        else{
            monthCard.parentNode.nextElementSibling.style.display='none';
        }
        if(cvc.value==''){
            cvc.nextElementSibling.style.display='block';
        }
        else{
            cvc.nextElementSibling.style.display='none';
        }
    }
    else{
        datos.nameCard=nameCard.value;
        datos.numberCard=numberCard.value;
        datos.monthCard=monthCard.value;
        datos.yearCard=yearCard.value;
        datos.cvc=cvc.value;
        creditCard.push(JSON.parse(JSON.stringify(datos)));
        nameCard.nextElementSibling.style.display="none";
        numberCard.nextElementSibling.style.display="none";
        monthCard.parentNode.nextElementSibling.style.display="none";
        cvc.nextElementSibling.style.display="none";

        form.style.display='none';
        complete.style.display='block';
    }
});

cont.addEventListener('click',()=>{
    form.style.display='block'
    complete.style.display='none'
    nameCard.value='';
    numberCard.value='';
    monthCard.value='';
    yearCard.value='';
    cvc.value='';
    frontCard.innerHTML='0000 0000 0000 0000';
    nameCardFront.innerHTML='jane appleseed';
    monthCardFront.innerHTML='00';
    yearCardFront.innerHTML='00';
    backCardcvc.innerHTML='000';
});

/*Actualizacion de tarjeta*/

function actualizarDatos(oldValue,newValue){
    oldValue.innerHTML=newValue;
};

nameCard.addEventListener('keyup',()=>{
    actualizarDatos(nameCardFront,nameCard.value);
});

numberCard.addEventListener('keyup',()=>{
    actualizarDatos(frontCard,numberCard.value);
});

monthCard.addEventListener('keyup',()=>{
    actualizarDatos(monthCardFront,monthCard.value);
});

yearCard.addEventListener('keyup',()=>{
    actualizarDatos(yearCardFront,yearCard.value);
});

cvc.addEventListener('keyup',()=>{
    actualizarDatos(backCardcvc,cvc.value);
});


/*Limitar caracteres number*/
function maxlength(obj){
    if(obj.value>obj.maxLength){
        obj.value=obj.value.slice(0,obj.maxLength)
    }
};